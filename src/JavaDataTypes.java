public class JavaDataTypes {
    public static void main(String[] args) {
        byte myNum = 100;
        System.out.println(myNum);
        short myNum2 = 5000;
        System.out.println(myNum);
        int myNum3 = 100000;
        System.out.println(myNum);
        long myNum4 = 15000000000L;
        System.out.println(myNum); 
        float myNum5 = 5.75f;
        System.out.println(myNum);
        double myNum6 = 19.99d;
        System.out.println(myNum);
        boolean isJavaFun = true;
        boolean isFishTasty = false;
        System.out.println(isJavaFun);     // Outputs true
        System.out.println(isFishTasty);   // Outputs falsec
       
    }
    
}
