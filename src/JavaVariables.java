public class JavaVariables {
    public static void main(String[] args) {
        String name = "Beer";
        System.out.println(name);
        int myNum = 15;
        System.out.println(myNum);
        float myFloatNum = 5.99f;
        System.out.println(myFloatNum);
        char myLetter = 'D';
        System.out.println(myLetter);
        boolean myBool = true;
        System.out.println(myBool);

    }
    
}
